/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.invoke;

import java.util.*;
import java.util.HashMap;
import java.util.Map;
import org.onemind.commons.java.lang.reflect.ReflectUtils;
/**
 * An invocable that contain a list of invocable functions to be invoke upon. In current implementation, it can only contains on
 * function with the specific name (regardless of arg types)
 * TODO: improve to allow multiple functions with same name	(different args)
 * @todo improve to allow multiple functions with same name (different args)
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: AbstractInvocable.java,v 1.1 2005/01/24 05:51:36 thlee Exp $ $Name:  $
 * @todo extend to allow multiple method with same name but different arg types
 * TODO extend to allow multiple method with same name but different arg types
 */
public abstract class AbstractInvocable implements Invocable
{

    /** the functions **/
    private Map _functions = new HashMap();

    /**
     * Add an function
     * @param functionName the function name
     * @param function the function
     */
    public void addFunction(InvocableFunction function)
    {
        Object key = ReflectUtils.toMethodString(function.getName(), function.getArgTypes());
        _functions.put(key, function);
    }

    /** 
     * {@inheritDoc}
     */
    public InvocableFunction getFunction(String functionName, Object[] args)
    {
        return getFunction(functionName, ReflectUtils.toArgTypes(args));
    }

    /** 
     * {@inheritDoc}
     */
    public InvocableFunction getFunction(String functionName, Class[] argTypes)
    {
        Object key = ReflectUtils.toMethodString(functionName, argTypes);
        InvocableFunction func = (InvocableFunction) _functions.get(key);
        if (func != null)
        {
            return func;
        } else
        {
            //search for it
            Iterator it = _functions.values().iterator();
            while (it.hasNext())
            {
                func = (InvocableFunction) it.next();
                if (functionName.equals(func.getName()) && func.canInvokeOn(argTypes))
                {
                    return func;
                }
            }
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean canInvoke(String functionName, Object[] args)
    {
        InvocableFunction f = getFunction(functionName, args);
        return (f != null);
    }

    /**
     * {@inheritDoc}
     */
    public Object invoke(String functionName, Object[] args) throws Exception
    {
        InvocableFunction function = getFunction(functionName, args);
        if (function != null)
        {
            return function.invoke(this, args);
        } else
        {
            throw new NoSuchMethodException("Method " + ReflectUtils.toMethodString(functionName, args) + " not found for " + this);
        }
    }

    /**
     * Get all the functions
     * @return the functions
     */
    public Collection getFunctions()
    {
        return Collections.unmodifiableCollection(_functions.values());
    }
}